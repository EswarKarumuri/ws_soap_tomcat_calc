package com.example.impl;

import javax.jws.WebService;

import com.example.Calc;

@WebService(serviceName="CalculatorService", name="Calculator", portName="CalculatorPort"
, endpointInterface="com.example.Calc", targetNamespace="http://www.com.ws.example")
public class CalcImpl implements Calc {

	@Override
	public int add(int a, int b) {
		return a+b;
	}

}
